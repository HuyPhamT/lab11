#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Load the dictionary of words from the given filename.
// Return a pointer to the array of strings.
// Sets the value of size to be the number of valid
// entries in the array (not the total array length).
char ** loadDictionary(char *filename, int *size)
{
	FILE *in = fopen(filename, "r");
	if (!in)
	{
	    perror("Can't open dictionary");
	    exit(1);
	}
	
	// TODO
	// Allocate memory for an array of strings (arr).
	int arrayLength = 10;
	int entries = 0; 
	char **arr = malloc(arrayLength * sizeof(char *));
	
	// Read the dictionary line by line.
	char line[20];
    while (fgets(line, 20, in) != NULL)
    {
    	// Trim a new line character
    	char *nl = strchr(line, '\n');
        if (nl != NULL)
        {
            *nl = '\0';
        }
        
    	if (arrayLength == entries)
    	{
    	// Expand array if necessary (realloc).
    	arrayLength += 10;
    	arr = realloc(arr, arrayLength * sizeof(char *));
    	}
    	// Allocate memory for the string (str).
    	char *s = malloc(20 * sizeof(char));
		// Copy each line into the string (use strcpy).
		strcpy(s, line);
		// Attach the string to the large array (assignment =).
		arr[entries] = s; 
		entries++;
    }
	
	
	
	// The size should be the number of entries in the array.
	*size = entries;
	
	// Return pointer to the array of strings.
	return arr;
}

// Search the dictionary for the target string
// Return the found string or NULL if not found.
char * searchDictionary(char *target, char **dictionary, int size)
{
    if (dictionary == NULL) return NULL;
    
	for (int i = 0; i < size; i++)
	{
	    if (strcmp(target, dictionary[i]) == 0)
	    {
	        return dictionary[i];
	    }
	}
	return NULL;
}